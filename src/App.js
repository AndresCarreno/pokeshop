

import { useEffect , useState } from 'react';



function App() {

  const [listPokemon, setListPokemon] = useState([]);
  const [pagePokemon, setPagePokemon] = useState({
    next: null, previous: null, totalPages: null, curPage: null
  })
  useEffect(()=>{
    fetchPokemon();

  },[])

  const fetchPokemon = async () =>{
    const req =  await window.fetch('https://pokeapi.co/api/v2/pokemon/')
    const {results, next, previous, count} = await req.json();

    let totalPages = count / 20 ;
    setListPokemon(results);
    setPagePokemon({next: next, previous: previous, totalPages: totalPages})


  }

  const prevPage = async () => {
    const req =  await window.fetch(pagePokemon.previous)
    const {results, next, previous} = await req.json();
    setListPokemon(results);
    setPagePokemon({ ...pagePokemon,next: next, previous: previous})

  }

  const nextPage = async () => {
    const req =  await window.fetch(pagePokemon.next)
    const {results, next, previous} = await req.json();
    setListPokemon(results);
    setPagePokemon({ ...pagePokemon, next: next, previous: previous})
  }
  const pokeList = listPokemon.map(p => (<li>{p.name}</li>))

  return (
    <div className="container">
      <center>POKEMON</center>
      <div className=''>
        <ul>
          {pokeList}
        </ul>
        <div className='row'>
          <div className='col-md-4'>
            <a className='btn btn-success' onClick={prevPage}>prev</a>
          </div>
          <div className='col-md-4'>
            <center>?/{pagePokemon.totalPages}</center>
          </div>
          <div className='col-md-4'>
            <a className='btn btn-success' onClick={nextPage}>next</a>
          </div>
        </div>
        
      </div>
    </div>
  );
}

export default App;
